import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;



class InsertUserPage extends StatefulWidget {

  InsertUserPage(this.map);

  Map? map;

  @override
  State<InsertUserPage> createState() => _InsertUserPageState();
}

class _InsertUserPageState extends State<InsertUserPage> {
  var formkey = GlobalKey<FormState>();

  TextEditingController nameController = TextEditingController();

  TextEditingController dobController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nameController.text = widget.map ==  null ? "":widget.map!["studentName"];
    dobController.text = widget.map ==  null ? "":widget.map!["createdAt"];
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Form(
        key: formkey,
        child: Column(
          children: [
            TextFormField(
              controller: nameController,
              decoration: InputDecoration(
                hintText: "enter student name:"
              ),
              validator: (value) {
                if(value!.isEmpty){
                  return "enter valid name";
                }
              },
            ),
            TextFormField(
              controller: dobController,
              decoration: InputDecoration(
                hintText: "enter dob:"
              ),
              validator: (value) {
                if(value!.isEmpty){
                  return "enter valid date";
                }
              },
            ),
            TextButton(
              onPressed: () {
                if(formkey.currentState!.validate()){
                  if(widget.map == null){
                    addStudent().then((value) => Navigator.of(context).pop(true));
                  }
                  else{
                    updateStudent(widget.map!["id"]).then((value) => Navigator.of(context).pop(true));
                  }
                }
              },
              child: Text("submit"),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> addStudent() async {
    Map map = {};
    map["studentName"] = nameController.text;
    map["createdAt"] = dobController.text;

    var response1 = await http.post(Uri.parse("https://637f92f52f8f56e28e907e7b.mockapi.io/students"),body: map);
  }

  Future<void> updateStudent(id) async {
    Map map = {};
    map["studentName"] = nameController.text;
    map["createdAt"] = dobController.text;

    var response1 = await http.put(Uri.parse("https://637f92f52f8f56e28e907e7b.mockapi.io/students/$id"),body: map);
  }
}
