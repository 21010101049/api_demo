import 'dart:convert';

import 'package:api_demo/insert_student.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        actions: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                  return InsertUserPage(null);
                },)).then((value) => (value) {
                  if(value == true){
                    setState(() {

                    });
                  }
                });
              },
              child: Icon(Icons.add),
            ),
          ),
        ],
      ),
      body: FutureBuilder<http.Response>(
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<dynamic> datas = jsonDecode(snapshot.data!.body.toString());
            datas.reversed;
            return ListView.builder(
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                      return InsertUserPage(jsonDecode(snapshot.data!.body.toString())[index]);
                    },)).then((value) => (value) {
                      if(value == true){
                        setState(() {

                        });
                      }
                    });
                  },
                  child: Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          jsonDecode(snapshot.data!.body.toString())[index]
                                  ["studentName"]
                              .toString(),
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          ConvertDateFromString(
                              jsonDecode(snapshot.data!.body.toString())[index]
                                      ["createdAt"]
                                  .toString()),
                          style: TextStyle(fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                );
              },
              itemCount: jsonDecode(snapshot.data!.body.toString()).length,
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
        future: getApiData(),
      ),
    );
  }

  String ConvertDateFromString(String dateToFormat) {
    try{
      DateTime date =
      DateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'").parse(dateToFormat);
      return DateFormat("dd-MMM-yyyy").format(date);
    }catch(e){
      DateTime date =
      DateFormat("dd/MM/yyyy").parse(dateToFormat);
      return DateFormat("dd-MMM-yyyy").format(date);
    }
  }

  Future<http.Response> getApiData() async {
    var response = await http
        .get(Uri.parse("https://637f92f52f8f56e28e907e7b.mockapi.io/students"));
    return response;
  }
}
